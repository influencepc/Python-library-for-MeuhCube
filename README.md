# Python library for MeuhCube (PLFM)

## About the MeuhCube

[The MeuhCube](https://github.com/Vincent14/meuhcube) is an Open Source LED cube project based on Arduino. It aims to provide any needed resources to create your own cube, including the electronic schematics, a laser cutted case and the firmware. The Arduino sketch exposes a serial API over USB that this Python library implements.


## License

This is an Open Source project placed under the [CeCILL-C license](http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html), similar to the LGPL license and adapted to French law.

### What you're free to do with this project

  * It basically allows you to use, share, modify and redistribute this project, including commercial use, on any support.

### What you're.. obliged to do for any distribution

  * You must keep the file headers concerning the license.
  * You must quote a link referring to [the original project page](https://gitlab.com/influencepc/Python-library-for-MeuhCube) on your publication.
  * You must distribute this project under the same CeCILL-C license. This is not affecting the project in which you are going to include it.

I hope you enjoy it :)
