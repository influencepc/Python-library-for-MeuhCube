
# Code execution limited from file
if __name__ == '__main__':
  try:
    print("Connexion on " + serial_ports()[0] + ":")
    usb_link = serial.Serial(serial_ports()[0], 115200)
    print(usb_link.readline())
    print(get_version())
    load_default_parameters()
    save_current_parameters()

    print("Executing tests:")

    # Fixtures
    loops = 5
    duration = 0.5
    state = True
    seven_states = [True, True, True, False, True, False, False]
    fourty_nine_states = [
      [True, True, True, True, True, True, False],
      [True, True, True, True, True, False, False],
      [True, True, True, True, False, False, False],
      [True, True, True, False, False, False, False],
      [True, True, False, False, False, False, False],
      [True, False, False, False, False, False, False],
      [False, False, False, False, False, False, False],
    ]
    full_cube_states = 7 * [fourty_nine_states]

    # Automated tests
    # print('  * set_all()')
    # for _ in range(loops):
    #   set_all()
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * set_all(seven_states)')
    # for _ in range(loops):
    #   set_all(seven_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * set_voxel((1, 1, 1), state)')
    # for _ in range(loops):
    #   set_voxel((1, 1, 1), state)
    #   time.sleep(duration)
    #   state = not state
    # set_all(7 * [False])

    # print('  * set_line("x", 1, 1, seven_states)')
    # for _ in range(loops):
    #   set_line("x", 1, 1, seven_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * set_plan("xz", 1, seven_states)')
    # for _ in range(loops):
    #   set_plan("xz", 1, seven_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * set_plan("xz", 1, fourty_nine_states)')
    # for _ in range(loops):
    #   set_plan("xz", 1, fourty_nine_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * set_cube(full_cube_states)')
    # for _ in range(loops):
    #   set_cube(full_cube_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * rev_all()')
    # for _ in range(loops):
    #   rev_all()
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * rev_voxel((1, 1, 1))')
    # for _ in range(loops):
    #   rev_voxel((1, 1, 1))
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * set_line("x", 1, 1, seven_states)')
    # for _ in range(loops):
    #   set_line("x", 1, 1, seven_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * rev_line("x", 1, 1, seven_states)')
    # for _ in range(loops):
    #   rev_line("x", 1, 1, seven_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * rev_plan("xz", 1, seven_states)')
    # for _ in range(loops):
    #   rev_plan("xz", 1, seven_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * rev_plan("xz", 1, fourty_nine_states)')
    # for _ in range(loops):
    #   rev_plan("xz", 1, fourty_nine_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * rev_cube(full_cube_states)')
    # for _ in range(loops):
    #   rev_cube(full_cube_states)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * translate("y")')
    # set_plan("xz", 1, seven_states)
    # for _ in range(loops):
    #   translate("y")
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * translate("y", 1, True)')
    # set_plan("xz", 1, seven_states)
    # for _ in range(loops):
    #   translate("y", 1, True)
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * rotate("z")')
    # set_plan("xz", 1, seven_states)
    # for _ in range(loops):
    #   rotate("z")
    #   time.sleep(duration)
    # set_all(7 * [False])

    # print('  * mirror("z")')
    # set_plan("xz", 1, seven_states)
    # for _ in range(loops):
    #   mirror("z")
    #   time.sleep(duration)
    # set_all(7 * [False])

    print("Tests completed")
  except IndexError:
    print("USB connexion not available.")
